package com.javanight.demo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.javanight.demo.domain.Dinosaur;
import com.javanight.demo.domain.HeavyDinosaurCount;
import com.javanight.demo.service.DinoService;
import com.mongodb.client.result.DeleteResult;

import io.swagger.annotations.ApiOperation;
import javafx.util.Pair;

 
 
@RestController
@RequestMapping("/api")
public class DinoController {
 
    public static final Logger logger = LoggerFactory.getLogger(DinoController.class);
    
    @Autowired
    DinoService dinoService;
 
    
    /**
     * Update First Dinosaur
     * @param dino
     * @return
     */
    
    @ApiOperation(value = "Update First Dinosaur ()")
    @RequestMapping(value = "/dino", method = RequestMethod.PATCH)
    public ResponseEntity<String> updateFirst() {
       
        Pair<String,String> updateClause = new Pair<String, String>("peso", "200.0");
        Pair<String,String> queryClause = new Pair<String, String>("periodo", "Jurásico medio"); ;
        dinoService.updateFirst(updateClause, queryClause);
        return new ResponseEntity<String>(HttpStatus.OK);
    } 
    
    
    
    /**
     * Retrieve a Dinosaur by ID
     * @param id
     * @return
     */
    @ApiOperation(value = "Retrieve a Dinosaur by ID")
    @RequestMapping(value = "/dino/{id}", method = RequestMethod.GET)
    public ResponseEntity<Dinosaur> userById(@PathVariable String id) {
        Dinosaur dino = dinoService.findById(id);
        if (dino == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Dinosaur>(dino, HttpStatus.OK);
    }
    
    /**
     * Retrieve all Dinosaurs
     * @return
     */
    
    @ApiOperation(value = "Retrieve all Dinosaurs")
    @RequestMapping(value = "/dinos", method = RequestMethod.GET)
    public ResponseEntity<List<Dinosaur>> listAllDinos() {

    	List<Dinosaur> dinos = dinoService.findAll();
        if (dinos == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Dinosaur>>(dinos, HttpStatus.OK);
    }
    
    
    /**
     * Add a new Dinosaur
     * @param dino
     * @return
     */
    
    @ApiOperation(value = "Add a new Dinosaur")
    @RequestMapping(value = "/dino", method = RequestMethod.POST)
    public ResponseEntity<String> insert(@RequestBody Dinosaur dino) {
        if (dino == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        dinoService.insert(dino);
        return new ResponseEntity<String>(HttpStatus.OK);
    }
    
    /**
     * Save/Update dinosaur
     * @param dino
     * @return
     */
    
    @ApiOperation(value = "Save/Update dinosaur")
    @RequestMapping(value = "/dino", method = RequestMethod.PUT)
    public ResponseEntity<String> saveOrUpdate(@RequestBody Dinosaur dino) {
        if (dino == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        dinoService.save(dino);
        return new ResponseEntity<String>(HttpStatus.OK);
    } 

    /**
     * Update Many Dinosaur
     * @param dino
     * @return
     */
    
    @ApiOperation(value = "Update Many Dinosaurs")
    @RequestMapping(value = "/dinos", method = RequestMethod.PATCH)
    public ResponseEntity<String> updateMany() {
       
        Pair<String,String> updateClause = new Pair<String, String>("peso", "150.0");
        Pair<String,String> queryClause = new Pair<String, String>("hallazgo.pais", "Estados Unidos"); ;
        dinoService.updateMulti(updateClause, queryClause);
        return new ResponseEntity<String>(HttpStatus.OK);
    }   
    
    /**
     * Delete a Dinosaur
     * @param id
     * @return
     */
    @ApiOperation(value = "Delete a Dinosaur")
    @RequestMapping(value = "/dino/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable String id) {
        if (id == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        DeleteResult remove = dinoService.remove(id);
        if(remove!=null && remove.wasAcknowledged())
        	return new ResponseEntity<String>(HttpStatus.OK);
        else
        	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
    }     
    
    /**
     * Heavy Dinosaurs grouped by country
     * @param id
     * @return
     */
    @ApiOperation(value = " Heavy Dinosaurs grouped by country")
    @RequestMapping(value = "/dinos-by-group", method = RequestMethod.GET)
    public ResponseEntity<List<HeavyDinosaurCount>> grouped() {
        List<HeavyDinosaurCount> agregation = dinoService.dinosaurAggregation();
        if(agregation!=null)
        	return new ResponseEntity<List<HeavyDinosaurCount>>(agregation, HttpStatus.OK);
        else
        	return new ResponseEntity<List<HeavyDinosaurCount>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }     
    
    
    @ApiOperation(value = "Retrieve Dinosaurs by weight")
    @RequestMapping(value = "/dinos-by-weight", method = RequestMethod.GET)
    public ResponseEntity<List<Dinosaur>> listAllDinosaurs(
    		@RequestParam(name="gt", required=true) double gt, @RequestParam(name="lt", required=true) double lt) {
    	List<Dinosaur> countries = dinoService.dinosaursBetweenWeight(gt, lt);
        if (countries == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Dinosaur>>(countries, HttpStatus.OK);
    }
    
    @ApiOperation(value = "Retrieve dinosaurs by period")
    @RequestMapping(value = "/dinos-by-periods", method = RequestMethod.GET)
    public ResponseEntity<List<Dinosaur>> dinosaurs(
    		@RequestParam(name="period", required=true) String period) {
    	List<Dinosaur> dinosaurs = dinoService.findByPeriod(period);
        if (dinosaurs == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Dinosaur>>(dinosaurs, HttpStatus.OK);
    }    
    
    //^Abel - us$
    @ApiOperation(value = "Retrieve dinosaurs by name")
    @RequestMapping(value = "/dinos-byname", method = RequestMethod.GET)
    public ResponseEntity<List<Dinosaur>> dinosaursByName(
    		@RequestParam(name="regex_name", required=true) String regex_name) {
    	List<Dinosaur> dinosaurs = dinoService.findByNameRegex(regex_name);
        if (dinosaurs == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Dinosaur>>(dinosaurs, HttpStatus.OK);
    }    
}
