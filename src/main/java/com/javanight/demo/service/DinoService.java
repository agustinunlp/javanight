package com.javanight.demo.service;

import java.util.List;

import com.javanight.demo.domain.Dinosaur;
import com.javanight.demo.domain.HeavyDinosaurCount;
import com.mongodb.client.result.DeleteResult;

import javafx.util.Pair;

public interface DinoService {
	
	Dinosaur findById(String id);

	List<Dinosaur> findAll();
	
	void insert(Dinosaur dino);
	
	void save(Dinosaur dino);
	
	void updateFirst(Pair<String, String> updateClause, Pair<String, String> queryClause);
	
	void updateMulti(Pair<String, String> updateClause, Pair<String, String> queryClause);
	
	public List<HeavyDinosaurCount> dinosaurAggregation();
	
	DeleteResult remove(String id);
	
	void upsert();	
	
	List<Dinosaur> dinosaursBetweenWeight(double weightGT, double weightLT);

	List<Dinosaur> findByPeriod(String period);
	
	List<Dinosaur> findByNameRegex(String name);

	
	

}
