package com.javanight.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClient;


@Configuration
@PropertySource(value="classpath:application.properties")
public class MongoConfig{

	@Value( "${spring.data.mongodb.primary.database}" )
	private String primaryDatabase;

	@Value( "${spring.data.mongodb.secondary.database}" )
	private String secondaryDatabase;

	@Value( "${spring.data.mongodb.host}" )
	private String host;

	@Value( "${spring.data.mongodb.port}" )
	private Integer port;
	
	@Bean(name = {"dinosaurMongoTemplate", "mongoTemplate"})
	public MongoTemplate dinosaurMongoTemplate() throws Exception {
        MongoClient mongoClient = new MongoClient(host, port);
		return new MongoTemplate(new SimpleMongoDbFactory(mongoClient,
        		primaryDatabase));
	}	 	

	@Bean(name="countryMongoTemplate")
	public MongoTemplate countryMongoTemplate() throws Exception {
		return new MongoTemplate(new SimpleMongoDbFactory(new MongoClient(host,port),
				secondaryDatabase));
	}
}