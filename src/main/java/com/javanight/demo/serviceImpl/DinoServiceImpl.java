package com.javanight.demo.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javanight.demo.domain.Dinosaur;
import com.javanight.demo.domain.HeavyDinosaurCount;
import com.javanight.demo.repository.DinosaurQueryRepository;
import com.javanight.demo.repository.DinosaurRepository;
import com.javanight.demo.service.DinoService;
import com.mongodb.client.result.DeleteResult;

import javafx.util.Pair;

@Service
public class DinoServiceImpl implements DinoService{
	
	@Autowired
	private DinosaurRepository dinosaurRepository;
	
	@Autowired
	private DinosaurQueryRepository dinosaurQueryRepository;

	@Override
	public Dinosaur findById(String id) {
		return dinosaurRepository.findById(id);
	}

	@Override
	public List<Dinosaur> findAll() {
		return dinosaurRepository.findAll();
	}

	@Override
	public void insert(Dinosaur dino) {
		dinosaurRepository.insert(dino);		
	}

	@Override
	public void save(Dinosaur dino) {
		dinosaurRepository.save(dino);		
	}

	@Override
	public void updateFirst(Pair<String, String> updateClause, Pair<String, String> queryClause ) {
		dinosaurRepository.updateFirst(updateClause, queryClause);
	}

	@Override
	public void updateMulti(Pair<String, String> updateClause, Pair<String, String> queryClause) {
		dinosaurRepository.updateMulti(updateClause, queryClause);
	}


	@Override
	public DeleteResult remove(String id) {
		return dinosaurRepository.remove(id);		
	}

	@Override
	public List<HeavyDinosaurCount> dinosaurAggregation() {
		return dinosaurRepository.dinosaurAggregation();
	}
	
	@Override
	public void upsert() {
		// TODO Auto-generated method stub
		
	}	

	@Override
	public List<Dinosaur> dinosaursBetweenWeight(double weightGT, double weightLT) {
		return dinosaurQueryRepository.dinosaursBetweenWeight(weightGT, weightLT);
	}

	@Override
	public List<Dinosaur> findByPeriod(String period) {
		return dinosaurQueryRepository.findByPeriod(period);
	}

	@Override
	public List<Dinosaur> findByNameRegex(String name) {
		return dinosaurQueryRepository.findByNameRegex(name);
	}
	
	

}
