package com.javanight.demo.domain;

import java.util.Calendar;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "dinosaurs")
public class Dinosaur {
	@Id
	private String id;
	private String nombre;
	private double peso;
	@Field("dieta")
	private String alimentacion;
	private String periodo;
	private Hallazgo hallazgo;
	private String descripcion;	
	
	@Indexed(name="expirationDateIndex", expireAfterSeconds=30)
    Date expirationDate = Calendar.getInstance().getTime();
	
		
	public Dinosaur() {
		super();
	}
	
	public Dinosaur(String nombre, double peso, String alimentacion, String periodo, Hallazgo hallazgo,
			String descripcion) {
		super();		
		this.nombre = nombre;
		this.peso = peso;
		this.alimentacion = alimentacion;
		this.periodo = periodo;
		this.hallazgo = hallazgo;
		this.descripcion = descripcion;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public String getAlimentacion() {
		return alimentacion;
	}
	public void setAlimentacion(String alimentacion) {
		this.alimentacion = alimentacion;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public Hallazgo getHallazgo() {
		return hallazgo;
	}
	public void setHallazgo(Hallazgo hallazgo) {
		this.hallazgo = hallazgo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Override
	public String toString() {
		return "Dinosaur [id=" + id + ", nombre=" + nombre + ", peso=" + peso + ", alimentacion=" + alimentacion
				+ ", periodo=" + periodo + ", hallazgo=" + hallazgo + ", descripcion=" + descripcion + "]";
	}
	
}
