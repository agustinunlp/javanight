package com.javanight.demo.domain;

public class Hallazgo {

	private String pais;
	private String ciudad;
	private int año;	
	
	
	public Hallazgo() {
		super();
	}
	public Hallazgo(String pais, String ciudad, int año) {
		super();
		this.pais = pais;
		this.ciudad = ciudad;
		this.año = año;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public int getAño() {
		return año;
	}
	public void setAño(int año) {
		this.año = año;
	}
	@Override
	public String toString() {
		return "Hallazgo [pais=" + pais + ", ciudad=" + ciudad + ", año=" + año + "]";
	}
}
