package com.javanight.demo.domain;

public class HeavyDinosaurCount {
	private String pais;
	private long count;

	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}

}
