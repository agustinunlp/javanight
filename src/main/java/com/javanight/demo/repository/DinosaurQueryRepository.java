package com.javanight.demo.repository;

 
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.javanight.demo.domain.Dinosaur;

public interface DinosaurQueryRepository extends MongoRepository<Dinosaur, String> {
	
	@Query("{ 'peso' : { $gt: ?0, $lt: ?1 } }")
	List<Dinosaur> dinosaursBetweenWeight(double weightGT, double weightLT);  
	
	@Query(value="{ 'periodo' : ?0 }")
	List<Dinosaur> findByPeriod(String period);

	@Query(value="{'nombre' : {'$regex' : ?0 }}")
	List<Dinosaur> findByNameRegex(String name);	
 
}
