package com.javanight.demo.repository.impl;

import java.util.List;

//imports as static
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.javanight.demo.domain.Dinosaur;
import com.javanight.demo.domain.HeavyDinosaurCount;
import com.javanight.demo.repository.DinosaurRepository;
import com.mongodb.client.result.DeleteResult;
import javafx.util.Pair;

@Service
public class DinosaurRepositoryImpl implements DinosaurRepository{

	@Autowired
	@Qualifier("dinosaurMongoTemplate")
	private MongoTemplate dinosaurMongoTemplate;

	@Override
	public Dinosaur findById(String id) {
		return dinosaurMongoTemplate.findById(id, Dinosaur.class);
	}

	@Override
	public List<Dinosaur> findAll() {
		return dinosaurMongoTemplate.findAll(Dinosaur.class);
	}

	@Override
	public void insert(Dinosaur dino) {
		dinosaurMongoTemplate.insert(dino);		
	}

	@Override
	public void save(Dinosaur dino) {
		dinosaurMongoTemplate.save(dino);		
	}

	@Override
	public void updateFirst(Pair<String, String> updateClause, Pair<String, String> queryClause ) {
		Update update = new Update();
		update.set(updateClause.getKey(),updateClause.getValue());
		Query query = Query.query(Criteria.where(queryClause.getKey()).is(queryClause.getValue()));
		dinosaurMongoTemplate.updateFirst(query, update, Dinosaur.class);
		
	}

	@Override
	public void updateMulti(Pair<String, String> updateClause, Pair<String, String> queryClause) {
		Update update = new Update();
		update.set(updateClause.getKey(),Double.parseDouble(updateClause.getValue()));
		Query query = Query.query(Criteria.where(queryClause.getKey()).is(queryClause.getValue()));
		dinosaurMongoTemplate.updateMulti(query, update, Dinosaur.class);	
	}


	@Override
	public DeleteResult remove(String id) {
		Dinosaur dinoToDelete = dinosaurMongoTemplate.findById(id, Dinosaur.class);
		DeleteResult remove = null;
		if(dinoToDelete!=null){
			remove = dinosaurMongoTemplate.remove(dinoToDelete);			
		}
		return remove;	
	}

	@Override
	public List<HeavyDinosaurCount> dinosaurAggregation() {

		Aggregation agg = newAggregation(
			match(Criteria.where("peso").gt(4)),
			group("hallazgo.pais").count().as("count"),
			project("count").and("_id").as("pais"),
			sort(Sort.Direction.DESC, "count")
				
		);
		//Convert the aggregation result into a List
		AggregationResults<HeavyDinosaurCount> groupResults 
			= dinosaurMongoTemplate.aggregate(agg, Dinosaur.class, HeavyDinosaurCount.class);
		List<HeavyDinosaurCount> result = groupResults.getMappedResults();
		
		return result;
		
	}
	@Override
	public void upsert() {
				
	}

}
