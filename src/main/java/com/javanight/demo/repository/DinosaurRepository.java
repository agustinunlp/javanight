package com.javanight.demo.repository;

import java.util.List;

import com.javanight.demo.domain.Dinosaur;
import com.javanight.demo.domain.HeavyDinosaurCount;
import com.mongodb.client.result.DeleteResult;

import javafx.util.Pair;

public interface DinosaurRepository {

	Dinosaur findById(String id);

	List<Dinosaur> findAll();
	
	void insert(Dinosaur dino);
	
	void save(Dinosaur dino);
	
	void updateFirst(Pair<String, String> updateClause, Pair<String, String> queryClause);
	
	void updateMulti(Pair<String, String> updateClause, Pair<String, String> queryClause);
	
	public List<HeavyDinosaurCount> dinosaurAggregation();
	
	DeleteResult remove(String id);
	
	void upsert();
	

}
